package ro.orangetraining;

public class Hotel {

    public int price;
    public String name;
    public String owner;

    public String display(String name, String owner) {

        return "The hotel " + name + " has the owner " + owner + ".";
    }


}